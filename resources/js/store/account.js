export default {
    namespaced: true,
    state: {
        id: -1,
        name: '',
        email: '',
        authenticated: false,
        remember: false,
        remember_token: ''
    },
    getters: {
        isAuthenticated: state => {
            return state.authenticated;
        },
        name: state => {
            return state.name;
        },
        email: state => {
            return state.email;
        },
        id: state => {
            return state.id;
        }
    },
    mutations: {
        hydrate(state, payload) {
            Object.assign(state, payload);

            this.commit('account/setAuthenticated', true);
        },
        update(state, payload) {
            for (let key in payload) {
                if (payload.hasOwnProperty(key)) {
                    state[key] = payload[key];
                }
            }
        },
        setName(state, name) {
            state.name = name;
        },
        setEmail(state, email) {
            state.email = email;
        },
        setAuthenticated(state, bool) {
            state.authenticated = bool;
        },
        toggleAuthenticated(state) {
            state.authenticated = !state.authenticated;
        },
        clear(state) {
            for (let key in state) {
                if (state.hasOwnProperty(key)) {
                    state[key] = null;
                }
            }
        },
    },
    actions: {
        hydrate({commit, state}, payload) {
            return new Promise((resolve, reject) => {
                commit('hydrate', payload);

                resolve();
            });
        },
        logout({commit, state}) {
            return new Promise((resolve, reject) => {
                commit('clear');

                resolve();
            });
        },
        update({commit, state}, payload) {
            return new Promise((resolve, reject) => {
                for (let key in payload) {
                    if (payload.hasOwnProperty(key)) {
                        if (!(key in state)) {
                            return reject('No such key');
                        }
                    }
                }

                commit('update', payload);

                resolve();
            });
        },
        setName({commit, state}, name) {
            return new Promise((resolve, reject) => {
                commit('setName', name);

                resolve();
            });
        },
        setEmail({commit, state}, email) {
            return new Promise((resolve, reject) => {
                // TODO: Check integrity of email.
                commit('setEmail', email);

                resolve();
            });
        },
    }
};