import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { VueHammer } from 'vue2-hammer';
import axios from 'axios';
import moment from 'moment';
import Common from './common/export';

// Vue dependencies
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueHammer);

// Global classes.
window.axios = axios;
window.Vue = Vue;
window.Moment = moment;
window.Util = Common.Util;
window.Logger = Common.Logger;

// Global classes settings.
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
