import Vuex from 'vuex';

import account from './store/account';

// Global state/event bus
export default new Vuex.Store({
  debug: true,
  strict: true,
  modules: {
    account
  }
});