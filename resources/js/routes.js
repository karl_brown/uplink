import VueRouter from 'vue-router';
import store from './store';

// This could be cleaned up too, but it's fine for now.
// Its main use is just to ensure 'requiresAuth' is always set for each route.
class Route {
    constructor(path, component, name, meta = {}) {
        this.path = path;
        this.component = component;
        this.name = name;
        this.meta = meta;

        if (!('requiresAuth' in meta)) {
            this.meta.requiresAuth = true;
        }
    }
}

class Redirect {
    constructor(from, to, name, meta = {}) {
        this.path = from;
        this.redirect = to;
        this.name = name;
        this.meta = meta;

        if (!('requiresAuth' in meta)) {
            this.meta.requiresAuth = true;
        }
    }
}

const router = new VueRouter({
    routes: [
        new Route('/', require('./components/account/login'), 'login', {requiresAuth:false}),
        new Redirect('*', '/', '404')
    ]
});

router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        if (!store.getters['account/isAuthenticated']) {
            next('/login');
            return;
        }
    }

    // Always call next()! Continues onto next step.
    next();
});

export default router;