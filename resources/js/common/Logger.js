class Logger {
    log() {
        if (process.env.MIX_STAGE !== 'production') {
            console.log(...arguments);
        }
    }

    warn() {
        if (process.env.MIX_STAGE !== 'production') {
            console.warn(...arguments);
        }        
    }

    error() {
        if (process.env.MIX_STAGE !== 'production') {
            console.error(...arguments);
        }
    }
}

export default new Logger;