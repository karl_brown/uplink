<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Uplink</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
        integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">

    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app" class="flex flex--column">
        <transition name="fade" mode="out-in">
            <router-view></router-view>
        </transition>
    </div>

    <script src="/js/app.js"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZjUjr_tiOzAroe53oZFO2ng_S3aLyL9Y"></script> --}}
</body>
</html>